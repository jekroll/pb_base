#!/usr/bin/perl -w
use strict;

#The MIT License (MIT)
##
##Copyright (c) 2016 José Eduardo Kroll
##
##Permission is hereby granted, free of charge, to any person obtaining a copy
##of this software and associated documentation files (the "Software"), to deal
##in the Software without restriction, including without limitation the rights
##to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
##copies of the Software, and to permit persons to whom the Software is
##furnished to do so, subject to the following conditions:
##
##The above copyright notice and this permission notice shall be included in
##all copies or substantial portions of the Software.
##
##THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
##IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
##FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
##AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
##LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
##OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
##THE SOFTWARE

my %CONFIG = ();
my %MSFOLDERS = ();
mkdir 'tmp';
mkdir 'genomes';

open CNFIG_h, "<$ARGV[0]";
while (<CNFIG_h>) {
  chomp;
  my $linha = $_;
  next if (! $linha || $linha =~ /^#/);

  if ($linha =~ /^\s*(\S+)\s+(\S+)(?:\s+(\S+))?/) {
    my $option = $1;
    my $value = $2;
    my $value2 = $3 || '';

    if ($option =~ /(maxquant_(\S+)|mzIdent)/i) {
      my $suboption = lc $&;
      $suboption =~ s/maxquant_//;
      
      unless (-e $value2) {
        print "ERROR (File doesnt exist): $value2\n\n";
        exit;
      }

      push @{$MSFOLDERS{$value}{$suboption}}, $value2;
    
    } elsif ($option =~ /(?:db|pb)_(\S+)/) {
      push @{$CONFIG{$1}}, $value;
    }

  }
}
close CNFIG_h;

############# CHECK CONFIG INFO
##specie
my $specie = $CONFIG{'specie'}[0] || '';
unless ($specie) {
  print "ERROR (Specie not defined)\n\n";
  exit;
}

##zscore and msdump
my $zscore_opt = '';
my $msdump_opt = '';
my $mzident_opt = '';

my @MSSAMPLE = keys %MSFOLDERS;
unless (@MSSAMPLE) {
  print "ERROR (Samples not defined)\n\n";
  exit;
} else {
  foreach my $sample (@MSSAMPLE) {

    if (exists $MSFOLDERS{$sample}{mzident}) {
      $mzident_opt .= " \"$sample:" . join(",", @{$MSFOLDERS{$sample}{mzident}}) . "\"";

    } else {

      if (exists $MSFOLDERS{$sample}{evidence}) {
        $zscore_opt .= " \"$sample:" . join(",", @{$MSFOLDERS{$sample}{evidence}}) . "\"";
      } else {
        print "ERROR (Samples not defined): $sample --> maxquant_evidence\n\n";
        exit;
      }

      if (exists $MSFOLDERS{$sample}{msms}) {
        $msdump_opt .= " \"$sample:" . join(",", @{$MSFOLDERS{$sample}{msms}}) . "\"";
      } else {
        print "ERROR (Samples not defined): $sample --> maxquant_msms\n\n";
        exit;
      }
    }
  }
}

## disco
my $disco_opt = '';
unless (exists $CONFIG{proteins}) {
  print "ERROR (db_proteins not defined)\n\n";
  exit;
} else {
  foreach my $filecheck (@{$CONFIG{proteins}}) {
    unless (-e $filecheck) {
      print "ERROR (db_proteins does not exist): $filecheck\n\n";
      exit;
    }
  }

  $disco_opt = join(",", @{$CONFIG{proteins}});
}

########### download data
print "Downloading data...";
print `./internal/getrefspecie.pl $specie`;
print "Done\n";

if ( $mzident_opt ) {
  ########### MZIDENT
  print STDERR "Running mzident.pl...";
  `./internal/mzident.pl $mzident_opt`;
  print STDERR "Done\n";

} else {
  ########### ZSCORE
  print STDERR "Running zscore.pl...";
  `./internal/zscore.pl $zscore_opt`;
  print STDERR "Done\n";

  ########### MSDUMP
  print STDERR "Running msdump.pl...";
  print `./internal/msdump.pl $msdump_opt`;
  print STDERR "Done\n";
}

########### DISCOVER GENE
print STDERR "Running discovergene.pl...";
`./internal/discovergene.pl $specie "$disco_opt"`;
print STDERR "Done\n";

#########################
print STDERR "Running alignpep.pl...";
`./internal/alignpep.pl "$disco_opt"`;
print STDERR "Done\n";

print STDERR "Running protsalign.pl...";
`./internal/protsalign.pl $specie`;
print STDERR "Done\n";

my $outfolder = 'RESULTS';
if (exists $CONFIG{outfolder}) {
  $outfolder = ${$CONFIG{outfolder}}[0];
}
print STDERR "Saving results to $outfolder\n";

`rm -R $outfolder`;
`mkdir -p $outfolder/INPUT/RESULTS`;
`cp -R ./HTML/. $outfolder`;

print STDERR "Running parsefinal...";
`./internal/parsefinal.pl "$outfolder/INPUT/RESULTS"`;
print STDERR "Done\n";
