#!/usr/bin/perl -w
use strict;

#The MIT License (MIT)
###
###Copyright (c) 2016 José Eduardo Kroll 
###
###Permission is hereby granted, free of charge, to any person obtaining a copy
###of this software and associated documentation files (the "Software"), to deal
###in the Software without restriction, including without limitation the rights
###to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
###copies of the Software, and to permit persons to whom the Software is
###furnished to do so, subject to the following conditions:
###
###The above copyright notice and this permission notice shall be included in
###all copies or substantial portions of the Software.
###
###THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
###IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
###FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
###AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
###LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
###OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
###THE SOFTWARE


my %FILENAMES = ();
foreach my $fileaux (@ARGV) {
  my ($sample, $files) = split /:/, $fileaux;
  my @FILES = split /,/, $files;
  $FILENAMES{$sample} = \@FILES;
}


my %SPEC = ();
my %TMP = ();
my %TMPIon = ();
my %SAMPLES = ();

my @IONSTP = ();
my @MASSES = ();
my @INTENS = ();

my $IonIDX = 0;
my $peptide = '';

########## PARSE XML

foreach my $samplename (keys %FILENAMES) {
  foreach my $filename (@{$FILENAMES{$samplename}}) {

    open FILE, "<$filename";
    while (<FILE>) {
      chomp;
      my $linha = $_;

      if ($linha =~ /<SpectrumIdentificationItem\s*(.*?)>/) {
        my %PARAMS = %{ getParams( $1 ) };
        $peptide = $PARAMS{ peptide_ref } || '';
        $peptide =~ s/^([^_]+).*/$1/;

        @IONSTP = ();
        @MASSES = ();
        @INTENS = ();

        %TMP = ();
        $TMP{ Peptide } = $peptide;
        $SAMPLES{$peptide}{$samplename} = 0;

      } elsif ($linha =~ /<IonType\s*(.*?)>/) {
        my %PARAMS = %{ getParams( $1 ) };
        my $charge = $PARAMS{ charge };
        my @AApos = split /:/, $PARAMS{ index };

        %TMPIon = ();
        $TMPIon{ Charge } = $charge;
        $TMPIon{ AApos } = \@AApos;

      } elsif ($linha =~ /<FragmentArray\s*(.*?)\/>/) {
        my %PARAMS = %{ getParams( $1 ) };
        my @measure_ref = split /:/, $PARAMS{ measure_ref };
        my @values = split /:/, $PARAMS{ values };

        $TMPIon{ $PARAMS{ measure_ref } } = \@values;

      } elsif ($linha =~ /<cvParam\s*(.*?)\/>/) {
        my %PARAMS = %{ getParams( $1 ) };
        my $ionType = $PARAMS{ name };

        #b ion - H2O
        if ( $ionType =~ /frag::*(\S+):*ion:*(-:*\S+)?/ ) {
          my $ionType = $1;
          $ionType .= $2 if ( $2 );
          $ionType =~ s/://g;

          $TMPIon{ ionType } = $ionType;

        } elsif ( $ionType =~ /(Tandem:expect|MS-GF:SpecEValue|OMSSA:evalue)$/ ) {
          $TMP{ PEP } = $PARAMS{ value };

        } elsif ( $ionType =~ /(Tandem:hyperscore|PSM:score)$/ ) {
          $TMP{ SCORE } = $PARAMS{ value };
        }

      } elsif ($linha =~ /<\/IonType>/) {
        my $idx_aux = 0;

        foreach my $AAidx ( @{$TMPIon{ AApos }}) {
          my ($part1, $part2) = split /-/, $TMPIon{ ionType };
          my $IonType = "$part1$AAidx";

          $IonType .= "($TMPIon{ Charge }+)" if ( $TMPIon{ Charge } > 1);
          $IonType .= "-$part2" if ( $part2 );

          next if ( $IonType =~ /(precursor|immonium)/i );

          push @IONSTP, $IonType;
          push @MASSES, $TMPIon{ 'Measure_MZ' }->[ $idx_aux ];
          push @INTENS, $TMPIon{ 'Measure_Int' }->[ $idx_aux ];

          $idx_aux ++;
        }

      } elsif ($linha =~ /<\/SpectrumIdentificationItem>/) {
        if ($peptide && (! exists $SPEC{$peptide} || $TMP{ SCORE } > $SPEC{$peptide}{score})) {
          $SPEC{$peptide}{rawfile} = $samplename;
          $SPEC{$peptide}{pepscore} = $TMP{ PEP };
          $SPEC{$peptide}{score} = $TMP{ SCORE };
          $SPEC{$peptide}{matches} = join(";", @IONSTP);
          $SPEC{$peptide}{intensities} = join(";", @INTENS);
          $SPEC{$peptide}{masses} = join(";", @MASSES);
        }

      }

    }
    close FILE;

  }
}


#output
mkdir "tmp";
open MSDUMP, ">./tmp/msdump.txt";
open ZSCORES, ">./tmp/zscores.txt";

foreach my $peptide (keys %SPEC) {
  print MSDUMP $peptide . "\t" .
  $SPEC{$peptide}{pepscore} . "\t" .
  $SPEC{$peptide}{score} . "\t" .
  $SPEC{$peptide}{matches} . "\t" .
  $SPEC{$peptide}{intensities} . "\t" .
  $SPEC{$peptide}{masses} . "\t" .
  join(',', keys %{$SAMPLES{$peptide}}) . "\n";

  print ZSCORES
    $SPEC{$peptide}{rawfile} . "\t" .
    $peptide . "\t" .
    "0\t0\n";
}

close MSDUMP;
close ZSCORES;


#####################################################################

sub getParams {
  my $linha = shift;

  1 while ( $linha =~ s/(=\s*"[^"]*?)\s+([^"]*?")/$1:$2/ );
  my @ITEMS = split /\s+/, $linha;
  my %PARAMS = ();

  map {
  if ( /(\S+?)\s*=\s*"(.*?)"/ ) {
    $PARAMS{ $1 } = $2;
  }
  } @ITEMS;

  return \%PARAMS;
}
