#!/usr/bin/perl -w
use strict;

#The MIT License (MIT)
##
##Copyright (c) 2016 José Eduardo Kroll 
##
##Permission is hereby granted, free of charge, to any person obtaining a copy
##of this software and associated documentation files (the "Software"), to deal
##in the Software without restriction, including without limitation the rights
##to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
##copies of the Software, and to permit persons to whom the Software is
##furnished to do so, subject to the following conditions:
##
##The above copyright notice and this permission notice shall be included in
##all copies or substantial portions of the Software.
##
##THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
##IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
##FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
##AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
##LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
##OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
##THE SOFTWARE

my $specie = lc $ARGV[0];

##download peptides
foreach my $filename (`curl -l ftp://ftp.ensembl.org/pub/current_fasta/$specie/pep/ 2> /dev/null`) {
  chomp $filename;
  
  if ($filename =~ /\.pep\.all\.fa/) {
    unless (-e "./genomes/$specie.proteins") {
      `curl ftp://ftp.ensembl.org/pub/current_fasta/$specie/pep/$filename --output ./genomes/$specie.proteins.gz 2> /dev/null ; gunzip ./genomes/$specie.proteins.gz`;
    }

    last;
  }

}

##download genome
my $gendown = '';
foreach my $filename (`curl -l ftp://ftp.ensembl.org/pub/current_fasta/$specie/dna/ 2> /dev/null`) {
  chomp $filename;

  if ($filename =~ /\.dna_rm\.toplevel\.fa/) {
    $gendown = $filename;
  }
  elsif ($filename =~ /\.dna\.primary_assembly\.fa/) {
    $gendown = $filename;
    last;
  }
}

if ( (! -e "./genomes/$specie.genome" || ! -e "./genomes/$specie.genome.2bit") && $gendown) {
  unlink "./genomes/$specie.genome";
  `curl ftp://ftp.ensembl.org/pub/current_fasta/$specie/dna/$gendown --output ./genomes/$specie.genome.gz 2> /dev/null ; gunzip ./genomes/$specie.genome.gz`;
  print `./external/faToTwoBit ./genomes/$specie.genome ./genomes/$specie.genome.2bit`;
}
