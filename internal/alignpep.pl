#!/usr/bin/perl -w
use strict;

#The MIT License (MIT)
##
##Copyright (c) 2016 José Eduardo Kroll 
##
##Permission is hereby granted, free of charge, to any person obtaining a copy
##of this software and associated documentation files (the "Software"), to deal
##in the Software without restriction, including without limitation the rights
##to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
##copies of the Software, and to permit persons to whom the Software is
##furnished to do so, subject to the following conditions:
##
##The above copyright notice and this permission notice shall be included in
##all copies or substantial portions of the Software.
##
##THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
##IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
##FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
##AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
##LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
##OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
##THE SOFTWARE

my @FILENAMES = split /,/, $ARGV[0];

#### Load peptides
my %PEPTIDES = ();
my $pepname = 0;

open MSDUMP, "<./tmp/msdump.txt";
while (<MSDUMP>) {
  chomp;
  
  if (/^(\S+)/) {
    my $sequence = $1;
    next if (length $sequence < 4);

    $PEPTIDES{"P_" . $pepname} = $sequence;
    $pepname++;
  }
}
close MSDUMP;

#### Load proteins
my %PROTEINS = ();
my $protname = '';

foreach my $filename (@FILENAMES) {
  my $protname = '';

  open PROTS, "<$filename";
  while (<PROTS>) {
    chomp;
    my $linha = $_;

    if ($linha =~ /^>(\S+)/) {
      $protname = $1;
    }
    elsif ($linha =~ /^(\S+)/) {
      my $sequence = $1;
      $PROTEINS{$protname} .= $sequence;
    }
  }
  close PROTS;
}

### create sufix tree
my %PROTSUFIX = ();

foreach my $protnamesux (keys %PROTEINS) {
  my $protseqsux = $PROTEINS{$protnamesux};
  my $prot_size = length $protseqsux;

  while ($protseqsux =~ m/(R|K)/cgi) {
    my $pos = $-[0];
    last if ($pos + 4 > $prot_size);

    my $pepsufix = substr($protseqsux, $pos + 1, 4);
    $pepsufix =~ s/(I|L)/X/ig;
    $PROTSUFIX{$pepsufix}{$protnamesux} = 0;
  }

  my $M_init = substr($protseqsux, 0, 4);
  $M_init =~ s/(I|L)/X/ig;
  $PROTSUFIX{$M_init}{$protnamesux} = 0;

  $M_init = substr($protseqsux, 1, 4);
  $M_init =~ s/(I|L)/X/ig;
  $PROTSUFIX{$M_init}{$protnamesux} = 0;
}

#### align
unlink "./tmp/aligns.txt";
open ALIGN, ">./tmp/aligns.txt";

foreach my $pepname (keys %PEPTIDES) {
  my $pepseq = $PEPTIDES{$pepname};
  my $pepsux = substr($pepseq, 0, 4);
  (my $pepseq_regex = $pepseq) =~ s/X/[IL]/g;

  foreach my $protname (keys %{$PROTSUFIX{$pepsux}}) {
    my $protseq = $PROTEINS{$protname};

    if ($protseq =~ /($pepseq_regex)/i) {
      my $alignpos = $-[0] + 1;
      my $real_pepseq = $1;

      print ALIGN "$protname\t$alignpos\t$pepseq\t$real_pepseq\n";
    }
  }
}

close ALIGN;
