#!/usr/bin/perl -w
use strict;

#The MIT License (MIT)
##
##Copyright (c) 2016 José Eduardo Kroll 
##
##Permission is hereby granted, free of charge, to any person obtaining a copy
##of this software and associated documentation files (the "Software"), to deal
##in the Software without restriction, including without limitation the rights
##to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
##copies of the Software, and to permit persons to whom the Software is
##furnished to do so, subject to the following conditions:
##
##The above copyright notice and this permission notice shall be included in
##all copies or substantial portions of the Software.
##
##THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
##IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
##FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
##AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
##LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
##OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
##THE SOFTWARE

my %FILENAMES = ();
foreach my $fileaux (@ARGV) {
  my ($sample, $files) = split /:/, $fileaux;
  my @FILES = split /,/, $files;
  $FILENAMES{$sample} = \@FILES;
}

########## CREATE MSMSDUMP
my %SAMPLES = ();
my %SPEC = ();

foreach my $samplename (keys %FILENAMES) {
  foreach my $filename (@{$FILENAMES{$samplename}}) {
    chomp $filename;

    open SPECTRUM, "<$filename";
    while (<SPECTRUM>) {
      chomp;

      my @DATA = split /\t/, $_;
      next if ($DATA[38] eq 'Matches');
      next if ($DATA[48] eq '+');

      #my $sample = $DATA[0];
      (my $peptide = $DATA[3]) =~ s/(I|L)/X/ig;
      my $pepscore = $DATA[24];
      my $score = $DATA[25];
      my $matches = $DATA[38];
      my $intensities = $DATA[39];
      my $masses = $DATA[42];

      #my $samplename = &convert_sampname( $sample );
      $SAMPLES{$peptide}{$samplename} = 0;

      if (! exists $SPEC{$peptide} || $score > $SPEC{$peptide}{score}) {
        $SPEC{$peptide}{pepscore} = $pepscore;
        $SPEC{$peptide}{score} = $score;
        $SPEC{$peptide}{matches} = $matches;
        $SPEC{$peptide}{intensities} = $intensities;
        $SPEC{$peptide}{masses} = $masses;
      }
    }

    close SPECTRUM;
  }
}


###output
open MSDUMP, ">./tmp/msdump.txt";
foreach my $peptide (keys %SPEC) {
  print MSDUMP $peptide . "\t" .
  $SPEC{$peptide}{pepscore} . "\t" .
  $SPEC{$peptide}{score} . "\t" .
  $SPEC{$peptide}{matches} . "\t" .
  $SPEC{$peptide}{intensities} . "\t" .
  $SPEC{$peptide}{masses} . "\t" .
  join(',', keys %{$SAMPLES{$peptide}}) . "\n";
}
close MSDUMP;
