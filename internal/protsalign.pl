#!/usr/bin/perl -w
use strict;

#The MIT License (MIT)
##
##Copyright (c) 2016 José Eduardo Kroll
##
##Permission is hereby granted, free of charge, to any person obtaining a copy
##of this software and associated documentation files (the "Software"), to deal
##in the Software without restriction, including without limitation the rights
##to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
##copies of the Software, and to permit persons to whom the Software is
##furnished to do so, subject to the following conditions:
##
##The above copyright notice and this permission notice shall be included in
##all copies or substantial portions of the Software.
##
##THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
##IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
##FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
##AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
##LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
##OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
##THE SOFTWARE

my $specie = $ARGV[0];

####Globals
my %PROTS = ();
my %PEPS = ();
my %PEPgene = ();
my %GENEpos = ();

open PROTf, "<./tmp/protgens.txt";
while (<PROTf>) {
  chomp;
  my @DATA = split /\t/, $_;
  my $protname = $DATA[0];
  my $genename = $DATA[1];
  my $protseq = $DATA[4];
  
  my $score = $DATA[3];
  next unless ($score > 1);

  my @auxpos = split(/:/, $DATA[2]);
  next if ($auxpos[0] =~ /_/);
  my $genepos_ref = \@auxpos;

  $GENEpos{$genename} = $genepos_ref;
  $PROTS{$genename}{$protname} = $protseq;
}
close PROTf;


#Load PEPS!!!
open PROTf, "<./tmp/aligns.txt";
while (<PROTf>) {
  chomp;
  my @DATA = split /\t/, $_;
  my $protname = $DATA[0];
  my $alignpos = $DATA[1];
  my $pepseq = $DATA[2];
  my $pepseq_ori = $DATA[3];

  push @{$PEPS{$protname}{$pepseq}}, [$alignpos, $pepseq_ori];
}
close PROTf;


#################################################### process...
open ALIGNprot_h, ">./tmp/protsalign.txt";
open ALIGNERR, ">./tmp/proterr.txt";

foreach my $genename (sort keys %PROTS)
{
  ##### excluir redundantes
  my %UNIQUE_aux = ();
  my %UNIQUE = ();
  my @PROTLIST = keys %{$PROTS{$genename}}; 

  foreach my $protname (@PROTLIST) {
    my @PEPS_aux = ();
    foreach my $pepseq (keys %{$PEPS{$protname}}) {
      push @PEPS_aux, $pepseq;
      $PEPgene{$pepseq}{$genename} = 0;
    }

    next unless (@PEPS_aux);
    my $idpep = join ',', sort @PEPS_aux;
    push @{$UNIQUE_aux{$idpep}}, $protname;
  }

  # se forem perfeitamente iguais, seleciona uma por hierarquia (ordem alfabetica)
  foreach my $idpep (keys %UNIQUE_aux) {
    my $bestprot = '';
    foreach my $protname (@{$UNIQUE_aux{$idpep}}) {
      if (! $bestprot || $protname lt $bestprot) {
        $bestprot = $protname;
      }
    }

    $UNIQUE{$bestprot} = 0;
  }

  # remove not useful data
  foreach my $protname (@PROTLIST) {
    unless (exists $UNIQUE{$protname}) {
      delete $PROTS{$genename}{$protname};
    }
  }
  
  ##### seleciona maior numero hits
  my %CLUSTER_MTX = ();
  @PROTLIST = keys %{$PROTS{$genename}};

  foreach my $protname_A (@PROTLIST) {
    foreach my $pepseq_A (keys %{$PEPS{$protname_A}}) {
      foreach my $protname_B (@PROTLIST) {
        next if ($protname_A eq $protname_B);

        if (exists $PEPS{$protname_B}{$pepseq_A}) {
          $CLUSTER_MTX{$protname_A}{$protname_B}{EQUAL} ++;
        } else {
          $CLUSTER_MTX{$protname_A}{$protname_B}{DIFF} ++;
        }
      }

    }
  }
 
  ## remove prots com menor numero de peps iguais 
  my %PROTS_ORDER_aux = ();

  my %CLUSTER_RM = ();
  foreach my $protname_A (keys %CLUSTER_MTX) {
    foreach my $protname_B (keys %{$CLUSTER_MTX{$protname_A}}) {
      my $equal = $CLUSTER_MTX{$protname_A}{$protname_B}{EQUAL} || 0;
      my $diff = $CLUSTER_MTX{$protname_A}{$protname_B}{DIFF} || 0;

      $PROTS_ORDER_aux{$protname_A} += $equal;

      if ($equal > 0 && $diff == 0) {
        $CLUSTER_RM{$protname_A} = 0;
      }
    }
  }

  foreach my $protexclude (keys %CLUSTER_RM) {
    delete $PROTS{$genename}{$protexclude};
  }

  ##imprimir output por ordem de prots similares
  my @PROTS_ORDER = ();
  foreach my $protname_A (sort {$PROTS_ORDER_aux{$a} <=> $PROTS_ORDER_aux{$b}} keys %{$PROTS{$genename}}) {
    push @PROTS_ORDER, $protname_A;
  }

  ######## ALIGN PROTS TO GENOMEEEE!!!!
  unlink "./tmp/gene.fa";
  my ($chr_a, $start_a, $end_a, $strand_a) = @{$GENEpos{$genename}};
  `./external/twoBitToFa ./genomes/$specie.genome.2bit:$chr_a:$start_a-$end_a ./tmp/gene.fa`;

  ##rev strand
  if ($strand_a < 0) { 
    unlink "./tmp/gene_aux.fa";
    my $sequence = '';

    open GENE_new, ">./tmp/gene_aux.fa";
    open GENE_rev, "<./tmp/gene.fa";
    
    while (<GENE_rev>) {
      chomp;
      my $linha = $_;

      if ($linha =~ /^>/) {
        print GENE_new "$linha\n";
      } else {
        $sequence .= $linha;
      }
    }

    $sequence = reverse $sequence;
    $sequence =~ tr/ATCGatcg/TAGCtagc/;
    print GENE_new "$sequence\n";
    
    close GENE_rev;
    close GENE_new;
  
    `mv ./tmp/gene_aux.fa ./tmp/gene.fa`;
  }

  #go align!!!
  my $err_tobet = scalar @PROTS_ORDER;
  my $err_fail = 0;

  foreach my $protname (@PROTS_ORDER) {
    my $protseq = $PROTS{$genename}{$protname};
    my $protlength = length $protseq;

    unlink "./tmp/prot.fa";
    open PROTseq_f, ">./tmp/prot.fa";
    print PROTseq_f ">$protname\n$protseq\n";
    close PROTseq_f;

    my $alignredo = 1;
    while ($alignredo) {

      my $option = "-m protein2genome";
      if ($alignredo >= 2) {
        $option = "-E -m protein2genome:bestfit"; #second try
      }
      
      my $command = "./external/exonerate -q ./tmp/prot.fa -t ./tmp/gene.fa -C TRUE -n 1 --showalignment FALSE --ryo \"%V{#%Pqs\\t%Pts\\t%Pl\\t%Ptb\\t%Pte#}\" -Q protein -T dna $option";
      my $result = `$command`;
      $result =~ s/\n//mg;

      ##############parse results
      my $protinit = 0;
      my $protinit_aux = 0;
      my $protend_aux = 0;
      my $protalign = '';
      pos $result = 0;

      ($protinit_aux, $protend_aux) = $result =~ /vulgar:\s*\S+\s+(\d+)\s+(\d+)/m;
      if ($protinit_aux) {
        foreach my $Xpos (0 .. $protinit_aux - 1) {
          $protalign .= '=' . substr($protseq, $Xpos, 1) . '=';
        }

        $protinit_aux *= 3;
      }

      $protinit_aux = 0 unless ($protinit_aux);
      $protend_aux = 0 unless ($protend_aux);

      my $identity = 100 * ($protend_aux - $protinit_aux) / $protlength;
      if ($identity < 95) {
        $alignredo ++;

      } else {

        ##parse align
        while ($result =~ /#([^#])\t?(\S+?)\t([^\t]+?)\t(\d+)\t(\d+)#/cg) {
          my ($aa, $codon, $label, $start, $end) = ($1, $2, $3, $4, $5);

          if ($label eq 'match') {
            $protinit = $start unless ($protinit);
            $protalign .= "=$aa=";

          } elsif ($label eq 'intron') {
            $protalign .= '.';

          } elsif ($label =~ /[53]\'ss/) {
            $protalign .= '..';

          } elsif ($label eq 'gap') {
            if ($aa eq '-') {
              $protalign .= "---";
            } else {
              $protalign .= "+$aa+";
            }

          } elsif ($label eq 'split codon') {
            if ($aa eq '-') {
              $protalign .= '!!!';
            } else {
              my $codon_len = length($codon);
              my $splitB = lc($aa) x length($codon);
              my $splitA = lc($aa) x (3 - length($codon));

              $protalign .= $splitB;
              $protalign =~ s/!!!/$splitA/;
            }

          } elsif ($label eq 'frameshift') {
            $protalign .= '-' x length($codon);
          }
        }
      
      }

      ##redo if align fails
      if (!$protalign) {
        if ($alignredo <= 1) { 
          $alignredo ++;
          next;
        } else {
          $alignredo = 0;
        }
      } else {
        $alignredo = 0;
      }

      ##outputs...
      if ($protlength > $protend_aux) {
        foreach my $Xpos ($protend_aux .. $protlength - 1) {
          $protalign .= '=' . substr($protseq, $Xpos, 1) . '=';
        }
      }

      $protalign =~ s/(\.+)/'<' . length($1) . '>'/eg;
      $protinit -= $protinit_aux;
      if ($protinit < 0) {
        next;
      } else {
        (my $protalign_check = $protalign) =~ s/=X=//g;
        next unless ($protalign_check);
      }

      print ALIGNprot_h "$genename\t$protname\t$protinit\t$protalign\n";
      $err_fail ++;
    }
  }

  my $err_diff = $err_tobet - $err_fail;
  print ALIGNERR "$genename\terr: $err_diff / $err_tobet\n";
}

close ALIGNprot_h;
close ALIGNERR;
