#!/usr/bin/perl -w
use strict;

#The MIT License (MIT)
##
##Copyright (c) 2016 José Eduardo Kroll
##
##Permission is hereby granted, free of charge, to any person obtaining a copy
##of this software and associated documentation files (the "Software"), to deal
##in the Software without restriction, including without limitation the rights
##to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
##copies of the Software, and to permit persons to whom the Software is
##furnished to do so, subject to the following conditions:
##
##The above copyright notice and this permission notice shall be included in
##all copies or substantial portions of the Software.
##
##THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
##IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
##FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
##AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
##LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
##OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
##THE SOFTWARE

my %FILENAMES = ();
foreach my $fileaux (@ARGV) {
  my ($sample, $files) = split /:/, $fileaux;
  my @FILES = split /,/, $files;
  $FILENAMES{$sample} = \@FILES;
}

################## Load and preprocess data
my %INT = ();
unlink "./tmp/tmp_int.txt";
open TMP_INT, ">./tmp/tmp_int.txt";

foreach my $rawfile (keys %FILENAMES) {
  foreach my $filename (@{$FILENAMES{$rawfile}}) {
    my $column = 0;
    chomp $filename;

    open FILE, "<$filename";
    while (<FILE>) {
      chomp;
      my @DATA = split /\t/, $_;

      next if ($DATA[0] eq 'Sequence');
      unless ($column) {
        $column = $#DATA - 10;
      }

      my $intensity = $DATA[$column] || 0;
      next unless ($intensity =~ /^[0-9\.,]+$/);
      (my $peptide = $DATA[0]) =~ s/[IL]/X/g;

      print TMP_INT "$rawfile\t$intensity\t$peptide\n";
      $INT{$rawfile}{SUM} += $intensity;
      $INT{$rawfile}{LNS} ++;
    }
    close FILE;

  }
}

close TMP_INT;


### process each sample separately
unlink "./tmp/zscores.txt";
open ZFILE, ">./tmp/zscores.txt";

foreach my $rawfile (keys %INT) {
  my @INT_ensity = ();
  my $Ntotal = $INT{$rawfile}{LNS};
  my $Sum = $INT{$rawfile}{SUM};

  ####-- slow but uses low memory
  open TMP_INT, "<./tmp/tmp_int.txt";
  while (<TMP_INT>) {
    chomp;
    my ($rawfile_aux, $intensity, $peptide) = split /\t/, $_;
    if ($rawfile eq $rawfile_aux) {
      push @INT_ensity, [$intensity, $peptide];
      last if (scalar @INT_ensity == $Ntotal);
    }
  }
  close TMP_INT;

  #get min detectable intensity
  @INT_ensity = sort { $a->[0] <=> $b->[0]}  @INT_ensity;

  my $idx_detected = 0;
  foreach my $idx_int (0 .. $#INT_ensity) {
    if ($INT_ensity[$idx_int]->[0] > 0) {
      $idx_detected = $idx_int - 1;
      last;
    }
  }

  my $rand_uni = (scalar(@INT_ensity) - $idx_detected) * 0.05;

  ###fix zero intensities
  foreach my $int_idx (0 .. $idx_detected) {
    my $randInt = $INT_ensity[ 1 + $idx_detected + rand($rand_uni) ]->[0];
    $INT_ensity[$int_idx]->[0] = $randInt;
    $Sum += $randInt;
  }

  #####remove pep redundancy -- get median intensity
  my @INT_ensity_rm = ();
  my %REDzscore = ();

  foreach my $idx_int (0 .. $#INT_ensity) {
    my $redpep = $INT_ensity[$idx_int]->[1];
    my $redint = $INT_ensity[$idx_int]->[0];

    push @{$REDzscore{$redpep}}, $redint;
  }

  foreach my $peptide (keys %REDzscore) {
    my @orderZ = sort { $a <=> $b } @{$REDzscore{$peptide}};
    my $int_size = scalar @orderZ;
    my $median = 0;

    if ($int_size >= 2) {
      if ($int_size % 2 == 0) {
        my $pa = $int_size / 2;
        my $pb = $int_size / 2 + 1;
        $median = ( $orderZ[$pa - 1] + $orderZ[$pb - 1] ) / 2;

      } else {
        my $pa = ($int_size / 2) + 0.5;
        $median = $orderZ[$pa - 1];
      }
    } else {
      $median = $orderZ[0];
    }

    push @INT_ensity_rm, [log $median, $peptide];
  }

  $Ntotal = scalar @INT_ensity_rm;
  $Sum = 0;
  foreach my $refdata (@INT_ensity_rm) {
    $Sum += $refdata->[0];
  }

  #mean
  my $mean = $Sum / $Ntotal;

  ###STDEV
  my $stdev = 0;
  foreach my $refdata (@INT_ensity_rm) {
    $stdev += ($mean - $refdata->[0]) ** 2;
  }

  $stdev /= $Ntotal;
  $stdev = sqrt( $stdev );

  ############ TODO!!!!!!! errorrrrr ---> usando stddev e mena de outros.... grava isso em hash.... 

  #### ZSCORE
  my %zscores = ();
  foreach my $refdata (@INT_ensity_rm) {
    my ($intensity, $peptide) = @{$refdata};
    my $zscore = ($intensity - $mean) / $stdev;

    ### density plot
    my %density = ();
    my @orderZ = sort { $a <=> $b } @{$REDzscore{$peptide}};
    foreach my $intensity_dens ( @orderZ ) {
      my $zscore_dens = int( (( log( $intensity_dens ) - $mean ) / $stdev) * 10 ) / 10;
      
      unless (exists $density{ $zscore_dens }) {
        $density{ $zscore_dens } = 1;
      } else {
        $density{ $zscore_dens } ++;
      }
    }

    my $density_report = '';
    my @density_report_aux = ();
    foreach my $dens_key (sort {$a <=> $b} keys %density) {
      push @density_report_aux, "$dens_key:$density{ $dens_key }";
    }

    $density_report = join ",", @density_report_aux;

    ##
    print ZFILE "$rawfile\t$peptide\t$zscore\t$density_report\n";
  }
}

close ZFILE;
