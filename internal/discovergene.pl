#!/usr/bin/perl -w
use strict;

#The MIT License (MIT)
##
##Copyright (c) 2016 José Eduardo Kroll 
##
##Permission is hereby granted, free of charge, to any person obtaining a copy
##of this software and associated documentation files (the "Software"), to deal
##in the Software without restriction, including without limitation the rights
##to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
##copies of the Software, and to permit persons to whom the Software is
##furnished to do so, subject to the following conditions:
##
##The above copyright notice and this permission notice shall be included in
##all copies or substantial portions of the Software.
##
##THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
##IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
##FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
##AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
##LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
##OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
##THE SOFTWARE

my $specie = $ARGV[0];
my @FILENAMES = split /,/, $ARGV[1];

############################ LOAD GENES SEQS
my $pepsize = 9;
my $stepwindow = 3;

my %GENOME_POS = ();
my %GENOME_IDX = ();
my $gene = '';
my $chrpos = '';
my $buffer = '';
my $ptype = '';

open ENSB, "<./genomes/$specie.proteins";
while (<ENSB>) {
  chomp;
  my $linha = $_;

  if ($linha =~ /^>/) {
    if ($gene && $buffer) {
      my $protseq = $buffer;
      my $protlen = length($protseq) - 1;

      my $transpos = 0;
      while ($transpos <= $protlen - $pepsize) {
        my $pep_toidx = substr($protseq, $transpos, $pepsize);
        if (scalar keys %{$GENOME_IDX{$pep_toidx}} < 2) {
          $GENOME_IDX{$pep_toidx}{$gene} = 0;
        }
      
        $transpos += $stepwindow;
      }
    }

    $gene = '';
    $buffer = '';

    ($ptype)  = $linha =~ /transcript_biotype:\s*(\S+)/;
    ($gene)   = $linha =~ /gene_symbol:\s*(\S+)/;
    ($chrpos) = $linha =~ /chromosome:[^:]+:([^:]+:\d+:\d+:\S+)/;

    $ptype = '' unless ($ptype);
    $gene = '' unless ($gene);
    $chrpos = '' unless ($chrpos);
   
    next unless ($ptype eq 'protein_coding');

    my ($chr_n, $start_n, $end_n, $strand_n) = split /:/, $chrpos;
    if (! $chr_n || $chr_n =~ /_/) {
      next;
    }

    #find gene border limits
    if ($chrpos && exists $GENOME_POS{$gene}) {
      my ($chr_o, $start_o, $end_o, $strand_o) = @{$GENOME_POS{$gene}};

      $start_o = $start_n - 150 if ($start_n < $start_o);
      $end_o = $end_n + 150 if ($end_n > $end_o);
      $GENOME_POS{$gene} = [ $chr_o, $start_o, $end_o, $strand_o ];

    } else {
      if ($chrpos) {
        $GENOME_POS{$gene} = [ $chr_n, $start_n, $end_n, $strand_n ];
      }
    }

  } elsif ($linha =~ /(\S+)/) {
    $buffer .= $1;
  }

}
close ENSB;

if ($gene && $buffer) {
  my $protseq = $buffer;
  my $protlen = length($protseq) - 1;

  my $transpos = 0;
  while ($transpos <= $protlen - $pepsize) {
    my $pep_toidx = substr($protseq, $transpos, $pepsize);
    if (scalar keys %{$GENOME_IDX{$pep_toidx}} < 2) {
      $GENOME_IDX{$pep_toidx}{$gene} = 0;
    }

    $transpos += $stepwindow;
  }
}

#exclude redundant
foreach my $peptide (keys %GENOME_IDX) {
  if (scalar keys %{$GENOME_IDX{$peptide}} > 1) {
    delete $GENOME_IDX{$peptide};
  }
}

########################## Load proteins
my %PEPTIDES = ();

foreach my $filename (@FILENAMES) {
  $buffer = '';
  my $protname = '';

  open PROTS, "<$filename";
  while (<PROTS>) {
    chomp;
    my $linha = $_;

    if ($linha =~ /^>(\S+)/) {
      my $protname_aux = $1;

      if ($buffer) {
        my $protseq = $buffer;
        my $protlen = length($protseq) - 1;

        my $transpos = 0;
        foreach my $transpos (0 .. $protlen - $pepsize) {
          my $pep_toidx = substr($protseq, $transpos, $pepsize);

          if (exists $GENOME_IDX{$pep_toidx}) {
            $PEPTIDES{$pep_toidx}{$protname} = 0;
          }
        }
        $buffer = '';
      }

      $protname = $protname_aux;
    }
    elsif ($linha =~ /^(\S+)/) {
      $buffer .= $1;
    }
  }
  close PROTS;

  if ($buffer) {
    my $protseq = $buffer;
    my $protlen = length($protseq) - 1;

    my $transpos = 0;
    foreach my  $transpos (0 .. $protlen - $pepsize) {
      my $pep_toidx = substr($protseq, $transpos, $pepsize);

      if (exists $GENOME_IDX{$pep_toidx}) {
        $PEPTIDES{$pep_toidx}{$protname} = 0;
      }
    }
  }
}

####compare them
my %RESULT = ();
foreach my $peptide (keys %GENOME_IDX) {
  my @GENES = keys %{$GENOME_IDX{$peptide}};
  my @PROTS = keys %{$PEPTIDES{$peptide}};

  foreach my $protname (@PROTS) {
    foreach my $genename (@GENES) {
      unless (exists $RESULT{$protname}{$genename}) {
        $RESULT{$protname}{$genename} = 0;
      }

      $RESULT{$protname}{$genename} ++;
    }
  }
}

###output results -- select best gene
%GENOME_IDX = (); ##free buffs
%PEPTIDES = ();

## load prot seqs
my %PROTSEQS = ();

foreach my $filename (@FILENAMES) {
  my $protname = '';

  open PROTS, "<$filename";
  while (<PROTS>) {
    chomp;
    my $linha = $_;

    if ($linha =~ /^>(\S+)/) {
      $protname = $1;
    }
    elsif ($linha =~ /^(\S+)/) {
      $PROTSEQS{$protname} .= $1;
    }
  }
  close PROTS;
}

##print to file
unlink "./tmp/protgens.txt";
open GENF, ">./tmp/protgens.txt";
foreach my $protname (keys %RESULT) {
  my @BESTGEN = sort { $RESULT{$protname}{$b} <=> $RESULT{$protname}{$a} } keys %{$RESULT{$protname}};
  my $genename = $BESTGEN[0];
  my $score = $RESULT{$protname}{$genename};

  if ($genename && $score) {
    my $protseq = $PROTSEQS{$protname};
    next unless (exists $GENOME_POS{$genename});
    print GENF "$protname\t$genename\t" . join(':', @{$GENOME_POS{$genename}}) . "\t$score\t$protseq\n";

  } else {
    print GENF "$protname\t#UNK\t0\t0\t0\n";
  }
}
close GENF;
