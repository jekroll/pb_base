#!/usr/bin/perl -w
use strict;

#The MIT License (MIT)
##
##Copyright (c) 2016 José Eduardo Kroll 
##
##Permission is hereby granted, free of charge, to any person obtaining a copy
##of this software and associated documentation files (the "Software"), to deal
##in the Software without restriction, including without limitation the rights
##to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
##copies of the Software, and to permit persons to whom the Software is
##furnished to do so, subject to the following conditions:
##
##The above copyright notice and this permission notice shall be included in
##all copies or substantial portions of the Software.
##
##THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
##IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
##FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
##AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
##LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
##OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
##THE SOFTWARE

my $OUTFOLDER = $ARGV[0];
mkdir $OUTFOLDER;

###load zscores
my %ZSCORE = ();

my %SAMPlist = ();
my %SAMPlist_aux = ();

my $SAMPcount = 0;

open PROTf, "<./tmp/zscores.txt";
while (<PROTf>) {
  chomp;
  my @DATA = split /\t/, $_;
  my $sample = $DATA[0];
  my $peptide = $DATA[1];
  my $zscore = $DATA[2];
  my $intdist = $DATA[3];

  #if ( exists $SAMPlist_aux{$sample} ) {
  #  $sample = $SAMPlist_aux{$sample};
  #} else {
  #  $SAMPcount ++;
  #  $SAMPlist_aux{$sample} = $SAMPcount;
  #  $sample = $SAMPlist_aux{$sample};
  #}

  $ZSCORE{$peptide}{$sample} = [ $zscore, $intdist ];
}
close PROTf;


foreach my $sampname (keys %SAMPlist_aux) {
  my $sampval = $SAMPlist_aux{ $sampname };
  $SAMPlist{ $sampval } = $sampname;
}


### load msdata
my %SPEC = ();

open PROTf, "<./tmp/msdump.txt";
while (<PROTf>) {
  chomp;
  my @DATA = split /\t/, $_;
  my $peptide = $DATA[0];
  $SPEC{$peptide}{pepscore} = $DATA[1];
  $SPEC{$peptide}{score} = $DATA[2];
  $SPEC{$peptide}{matches} = $DATA[3];
  $SPEC{$peptide}{intensities} = $DATA[4];
  $SPEC{$peptide}{masses} = $DATA[5];
  $SPEC{$peptide}{samples} = $DATA[6];
}
close PROTf;

#open peptides
my %PEPS = ();

open PROTf, "<./tmp/aligns.txt";
while (<PROTf>) {
  chomp;
  my @DATA = split /\t/, $_;
  my $protname = $DATA[0];
  my $alignpos = $DATA[1];
  my $pepseq = $DATA[2];
  my $pepseq_ori = $DATA[3];

  push @{$PEPS{$protname}}, [$alignpos, $pepseq, $pepseq_ori];
}
close PROTf;

####### open aligns
my %RAWseqs = ();
my %SEQS_ALIGN = ();
my %MERGE = ();
my %INDELS = ();

my $protorde = 0;

open ALIGN_h, "<./tmp/protsalign.txt";
while (<ALIGN_h>) {
  chomp;
  my $linha = $_;
  my @DATA = split /\t/, $linha;
  my $genename = $DATA[0];
  my $protname = $DATA[1];
  my $protinit = $DATA[2];
  my $protsmall = $DATA[3];

  $protorde++;

  next unless ($protsmall);
  my $protalign = $protsmall;

  while ($protsmall =~ s/\+(.)\+//) {
    my $posaa = $-[0] + 1;
    $INDELS{$protname}{$posaa} = "IN";
  }

  pos $protsmall = 0;
  while ($protsmall =~ /-/cg) {
    my $posaa = $-[0] + 1;
    $INDELS{$protname}{$posaa} = "DEL";
  }

  ####
  $protsmall =~ s/([A-z=-]+)/"#E:" . length($1) . ":$1:E#"/ge;
  $protsmall =~ s/<(\d+)>/#I:$1:.:I#/g;
  $protsmall = "#I:$protinit:.:I#" . $protsmall;

  my @SEQITEMS = ();
  my $exon_start = 0;
  my $exon_end = 0;

  while ($protsmall =~ s/#([EI]):(\d+):(\S+?):[EI]#//) {
    my ($item, $size, $sequence) = ($1, $2, $3);
    $exon_start = $exon_end + 1; 
    $exon_end = $exon_start + ($size - 1);

    push @SEQITEMS, [$item, $exon_start, $exon_end, $sequence];
  }

  ### merge of gene exons -- store info
  unless ($MERGE{$genename}{aux}) {
    @{$MERGE{$genename}{aux}} = ();
  }
  my $merge_ref = $MERGE{$genename}{aux};

  if (scalar @{$merge_ref}) {

    foreach my $refdata (@SEQITEMS) {
      my ($item, $exon_start, $exon_end) = @{$refdata};

      if ($item eq 'E') {
        my $nonematch = 1;

        foreach my $pos_ref (@{$merge_ref}) {
          my ($merge_start, $merge_end) = @{$pos_ref};

          if ($exon_start < $merge_end && $exon_end > $merge_start) {
            $merge_start = $exon_start if ($exon_start < $merge_start);
            $merge_end = $exon_end if ($exon_end > $merge_end);

            $nonematch = 0;
          }

          @{$pos_ref} = ($merge_start, $merge_end);
        }

        push @{$merge_ref}, [$exon_start, $exon_end] if ($nonematch);
      }
    }

  } else {

    foreach my $refdata (@SEQITEMS) {
      my ($item, $exon_start, $exon_end) = @{$refdata};
      push @{$merge_ref}, [$exon_start, $exon_end] if ($item eq 'E');
    }

  }

  ##rec info
  $SEQS_ALIGN{$genename}{$protname} = [\@SEQITEMS, $protorde];
}
close ALIGN_h;

######  DO MERGE!!!
foreach my $genename (keys %MERGE) {
  my $merge_aux = $MERGE{$genename}{aux};
  my %MERGEnord = ();

  foreach my $pos_ref (@{$merge_aux}) {
    my ($merge_start, $merge_end) = @{$pos_ref};
    $MERGEnord{"$merge_start:$merge_end"} = 0;
  }

  @{$merge_aux} = ();

  foreach my $pos_hsh (keys %MERGEnord) {
    my ($merge_start, $merge_end) = split /:/, $pos_hsh;
    push @{$merge_aux}, [$merge_start, $merge_end];
  }

  my $exon_start_old = 0;
  my $exon_end_old = 0;

  unless (exists $MERGE{$genename}{final}) {
    @{$MERGE{$genename}{final}} = ();
  }

  my $merge_fnl = $MERGE{$genename}{final};

  foreach my $pos_ref ( sort { ${$a}[0] <=> ${$b}[0] } @{$merge_aux}) {
    my ($exon_start, $exon_end) = @{$pos_ref};

    push @{$merge_fnl}, ['I', $exon_end_old + 1, $exon_start - 1, ''];
    push @{$merge_fnl}, ['E', $exon_start, $exon_end, ''];

    $exon_start_old = $exon_start;
    $exon_end_old = $exon_end;
  }

  #clean
  delete $MERGE{$genename}{aux};
}

##### find peptides shared among genes
my %PEPSHARED = ();
my %PEPSCOUNT = ();

foreach my $genename (keys %SEQS_ALIGN) {
  foreach my $protname (keys %{$SEQS_ALIGN{$genename}}) {
    foreach my $pep_ref (@{$PEPS{$protname}}) {
      my $pepseq = $pep_ref->[1];
      $PEPSHARED{$pepseq}{$genename} = 0;
      $PEPSCOUNT{$genename}{$pepseq} = 0;
    }
  }
}

############ Comparations
my %SUMMARY = ();

foreach my $genename (keys %MERGE) {
  my @BUFF_hOUT = ();
  my %PRINT_BUFFSAMP = ();
  my %PRINT_BUFFSPEC = ();
  my @PRINT_BUFFPEPGENE = ();
  my @PRINT_BUFFGENEPEP = ();
  my %SHARED_GENES = ();

  my %PEPlist_aux = ();
  my %PEPlist = ();
  my $pepcount = 0;

  my $merged_ref = $MERGE{$genename}{final};
  my $seqsalign_ref = $SEQS_ALIGN{$genename};
  my @ORDER_PROTS = sort { ${$seqsalign_ref}{$a}->[1] <=> ${$seqsalign_ref}{$b}->[1] }  keys %{$seqsalign_ref};

  ### cmp1
  foreach my $protname (@ORDER_PROTS, 'merged') {
    my $refexon_pos1 = '';
    if ($protname eq 'merged') {
      $refexon_pos1 = $merged_ref;
    } else {    
      $refexon_pos1 = ${$seqsalign_ref}{$protname}->[0] || next;
    }
        
    my $protsequence = '';
    my $new_protalign = '';
    my $pos_offset = 0;

    foreach my $posses_ref1 (@{$refexon_pos1}) {
      my ($item1, $intron_start1, $intron_end1, $sequence) = @{$posses_ref1};;

      #### cmp2
      if ($item1 eq 'I') {
        my $discount = 0;

        foreach my $posses_merge (@{$merged_ref}) {
          my ($merge_attr, $merge_start, $merge_end) = @{$posses_merge};
          next if ($merge_attr ne 'E');

          if ($intron_start1 <= $merge_end && $intron_end1 >= $merge_start) {
            if    ($intron_start1 > $merge_start && $intron_end1 > $merge_end) { $discount += (1 + $merge_end - $intron_start1); } 
            elsif ($intron_start1 < $merge_start && $intron_end1 < $merge_end) { $discount += (1 + $intron_end1 - $merge_start); }
            elsif ($intron_start1 < $merge_start && $intron_end1 > $merge_end) { $discount += (1 + $merge_end - $merge_start);   }
            elsif ($intron_start1 > $merge_start && $intron_end1 < $merge_end) { $discount += (1 + $intron_end1 - $intron_start1); }  ##IRetention
          }
        }

        if ($discount > 0) {
          $protsequence .= '-' x $discount;

          my $pos_offset_aux = $pos_offset;
          $pos_offset += ($discount - 1);

          $new_protalign .= "I$pos_offset_aux-$pos_offset ";
          $pos_offset ++;
        }

        my $intron_size = 1 + $intron_end1 - $intron_start1;
        $new_protalign .= "K0-0-$intron_size ";
      }
      elsif ($item1 eq 'E') {
        $protsequence .= $sequence;

        my $pos_offset_aux = $pos_offset;
        $pos_offset += ($intron_end1 - $intron_start1);
        $new_protalign .= "E$pos_offset_aux-$pos_offset ";
        $pos_offset ++;
      }
    }

    $protsequence =~ s/([a-z])\1\1/"=" . uc($1) . "="/ge;

    ##### PROCESS PEPTIDES TO REPORT
    my @PEP_ALIGNS = ();

    foreach my $peps_ref (@{$PEPS{$protname}}) {
      my ($ini_pos, $pepseq, $pepseq_ori) = @{$peps_ref};
     
      my $pepseq_lnk = '';
      if ( exists $PEPlist_aux{$pepseq_ori} ) {
        $pepseq_lnk = "P['$PEPlist_aux{ $pepseq_ori }']";
      } else {
        $pepcount ++;
        $PEPlist_aux{ $pepseq_ori } = $pepcount;
        $pepseq_lnk = "P['$PEPlist_aux{ $pepseq_ori }']";
      }

      $ini_pos --;
      my $end_pos = $ini_pos + length($pepseq) - 1;
      $pepseq =~ s/-+//g;

      $ini_pos *= 3; $end_pos *= 3;
      
      #### check for INDELS and readjust
      my $ini_pos_aux = $ini_pos;
      my $end_pos_aux = $end_pos;

      foreach my $delpos (sort {$a <=> $b} keys %{$INDELS{$protname}}) {
        my $indel = $INDELS{$protname}{$delpos};

        if ($indel eq 'IN') {
          $ini_pos_aux -= 3 if ($ini_pos > $delpos);
          $end_pos_aux -= 3 if ($end_pos > $delpos);
        } elsif ($indel eq 'DEL') {
          $ini_pos_aux += 1 if ($ini_pos > $delpos);
          $end_pos_aux += 1 if ($end_pos > $delpos);
        }
      }

      $ini_pos = $ini_pos_aux;
      $end_pos = $end_pos_aux;

      my $AUX_protalign = $new_protalign;
      my $AUX_pepalign = "P$ini_pos-$end_pos-XXX!$pepseq_ori";

      while ($AUX_protalign =~ /I(\d+)-(\d+)/gci) {
        my ($intron_start, $intron_end) = ($1, $2);

        if ($ini_pos >= $intron_start) {
          my $intron_size = (1 + $intron_end - $intron_start);
          $ini_pos += $intron_size;
          $end_pos += $intron_size;
          $AUX_pepalign = "P$ini_pos-$end_pos-XXX!$pepseq_ori";

        } elsif ($end_pos >= $intron_start) {
          my $intron_size = (1 + $intron_end - $intron_start);
          $end_pos += $intron_size;
          $AUX_pepalign  = "P$ini_pos-$end_pos-$intron_start:$intron_end!$pepseq_ori";
        }
      }     

      push @PEP_ALIGNS, $AUX_pepalign;

      ##### PEPTIDE SPECTRUM
      my @PRINT_BUFF2 = ();
      my @MATCHES = split /;/, $SPEC{$pepseq}{matches}; 
      my @INTENSITIES = split /;/, $SPEC{$pepseq}{intensities}; 
      my @MASSES = split /;/, $SPEC{$pepseq}{masses}; 

      ######### expression z-score
      my @SAMPSaux = sort keys %{$ZSCORE{$pepseq}};
      foreach my $sampidx ( 0 .. $#SAMPSaux) {
        my $zscoreaux = sprintf("%.2f", $ZSCORE{ $pepseq }{ $SAMPSaux[ $sampidx ] }->[ 0 ]);
        my @INTDENS_aux = split /,/, $ZSCORE{ $pepseq }{ $SAMPSaux[ $sampidx ] }->[ 1 ];
        
        my @INTDENS = ();
        foreach my $intden (@INTDENS_aux) {
          my @DEN = split /:/, $intden;
          $DEN[1] = 0 unless ( defined $DEN[1] );
          
          push @INTDENS, "'" . sprintf("%.2f", $DEN[0]) . ":" . $DEN[1] . "'";
        }

        #map { $_ = "'" . sprintf("%.2f", $_) . "'"; } @INTDENS;
        $SAMPSaux[ $sampidx ] = "['$SAMPSaux[ $sampidx ]','$zscoreaux',[" . join(',', @INTDENS) . "]]";
      }

      my $samps = join(', ', @SAMPSaux);
      $PRINT_BUFFSAMP{ $pepseq_ori } = "[$samps]";

      foreach my $pos (0 .. $#MATCHES) {
        my $match = $MATCHES[$pos];
        my $intensity = sprintf("%.1f", $INTENSITIES[$pos]);
        my $mass = sprintf("%.1f", $MASSES[$pos]);

        push @PRINT_BUFF2, "['$match','$mass','$intensity']";
      }

      ##push scores
      my $score = $SPEC{$pepseq}{score};
      my $pepscore = $SPEC{$pepseq}{pepscore};
      push @PRINT_BUFF2, "['scores','$score','$pepscore']";

      ##shared pep genes
      my @SHARED_GENES_aux = keys %{$PEPSHARED{$pepseq}};
      foreach my $sharedgene_aux (@SHARED_GENES_aux) {
        $SHARED_GENES{$sharedgene_aux} = 0;
      }

      map { $_ = "'" . $_ . "'" } @SHARED_GENES_aux;

      my @SHRaux = ();
      foreach my $geneSHR (@SHARED_GENES_aux) {
        if ("'$genename'" ne $geneSHR) {
          push @SHRaux, $geneSHR;
        }
      }

      if (scalar @SHRaux) {
        push @PRINT_BUFFPEPGENE, "'$pepseq_ori':[" . join(',', @SHRaux) . "]";
      }

      #join final
      my $printbuff2 = join ',', @PRINT_BUFF2;
      $PRINT_BUFFSPEC{ $pepseq_ori } = "[$printbuff2]";
    }

    #### FINAL REPORT
    my $peps_toreport = join ' ', @PEP_ALIGNS;

    ### ADD TO BUFF
    (my $protname_aux = $protname) =~ s/^[A-Z]://;
    push @BUFF_hOUT, "{seqname: '$protname_aux', realname: '$protname_aux', structure: '$new_protalign$peps_toreport', sequence: '$protsequence'}";
  }

  next unless ( scalar @BUFF_hOUT > 1 );
  #my $printbuff1 = join ",", @PRINT_BUFFSPEC;
  #my $printbuffexp = join ",", @PRINT_BUFFSAMP;
  my $printbuffpgene = join ",", @PRINT_BUFFPEPGENE;

  #shared pep genes
  my @SHARED_GENES_aux = sort keys %SHARED_GENES;
  foreach my $shared_genename (@SHARED_GENES_aux) {
    my $genepep_count = scalar keys %{$PEPSCOUNT{$shared_genename}};
    push @PRINT_BUFFGENEPEP, "'$shared_genename':'$genepep_count'";
  }

  my $printbuffgpep = join ",", @PRINT_BUFFGENEPEP;

  
  ###prepare list of peps reduced
  #$PEPlist_aux{ $pepseq_ori } = $pepcount
  #foreach my $pepname (keys %PEPlist_aux) {
  #  my $pepval = $PEPlist_aux{ $pepname };
  #  $PEPlist{ $pepval } = $pepname;
  #}

  #my $printbuff_peplist = '';
  #my @buff_peplist = ();

  #foreach my $pepname (keys %PEPlist ) {
  #  my $pepval = $PEPlist{ $pepname };
  #  push @buff_peplist, "'$pepname':'$pepval'";
  #}
  #$printbuff_peplist = join(",", @buff_peplist);

  ###prepare list of samples reduced
  #my $printbuff_samplist = '';
  #my @buff_samplist = ();

  #foreach my $sampname (keys  %SAMPlist) {
  #  my $sampval = $SAMPlist{ $sampname };
  #  push @buff_samplist, "'$sampname':'$sampval'";
  #}
  #$printbuff_samplist = join(",", @buff_samplist);
  
  ###report!!
  my $filename = "$OUTFOLDER/$genename/PROTB_$genename.tmp";
  mkdir "$OUTFOLDER/$genename";

  open hOUT, ">$filename.1";
  print hOUT "console.log('Evaluated $genename');\nGLOBAL_aligns = [\n";
  print hOUT join ",\n", @BUFF_hOUT;
  print hOUT "\n];\n\n";
  close hOUT;
 
  #open hOUT, ">$filename.2";
  #print hOUT "S = { $printbuff_samplist };\n\n";
  #print hOUT "P = { $printbuff_peplist };\n\n";
  #close hOUT;

  foreach my $pepspec ( keys %PRINT_BUFFSPEC ) {
    open hOUT, ">$filename.2_$pepspec";
    print hOUT "\$.extend( GRAPH_DATA, { '$pepspec': $PRINT_BUFFSPEC{ $pepspec } });\n\n";
    close hOUT;

    `rm -f $filename.2_$pepspec.lzma ; xz --format=lzma -ze $filename.2_$pepspec`;
  }

  foreach my $pepspec ( keys %PRINT_BUFFSAMP ) {
    open hOUT, ">$filename.3_$pepspec";
    print hOUT "\$.extend( SAMPLESEX, { '$pepspec': $PRINT_BUFFSAMP{ $pepspec } });\n\n";
    close hOUT;

    `rm -f $filename.3_$pepspec.lzma ; xz --format=lzma -ze $filename.3_$pepspec`;
  }

  open hOUT, ">$filename.4";
  print hOUT "PEPGENES = { $printbuffpgene };\n\n";
  
  open hOUT, ">$filename.5";
  print hOUT "GENEPEPS = { $printbuffgpep };\n";
  close hOUT;

  #summary
  $SUMMARY{$genename}{isoforms} = scalar(@BUFF_hOUT) - 1;
  $SUMMARY{$genename}{peptides} = scalar keys %PRINT_BUFFSPEC;

  ##compress info
  foreach my $part (1, 4, 5) {
    `rm -f $filename.$part.lzma ; xz --format=lzma -ze $filename.$part`;
  }
}



######## Create SUMMARY
my @SUMBUFF = ();
foreach my $gene (sort keys %SUMMARY) {
  push @SUMBUFF, "{ gene: '$gene', nisoforms: $SUMMARY{$gene}{isoforms}, npeptides: $SUMMARY{$gene}{peptides} }";
}

open hSUM, ">$OUTFOLDER/summary.js";
print hSUM "var mydata = [ " . join(",\n", @SUMBUFF) . " ];";
close hSUM;
